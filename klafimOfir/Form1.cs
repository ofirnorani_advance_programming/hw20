﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace klafimOfir
{
    public partial class Form1 : Form
    {
        const int numOfImg = 10;
        public Form1()
        {
            InitializeComponent();
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            GenerateCards();
        }

        public void GenerateCards()
        {
            for(int i = 0; i<numOfImg; i++)
            {
                PictureBox picture = new PictureBox
                {
                    Name = "PB" + i.ToString(),
                    Size = new Size(80, 105),
                    Location = new Point(10 + i * 95, 365),
                    Image = klafimOfir.Properties.Resources.card_back_red,
                };
                picture.Click += new EventHandler(ImageEvent);
                picture.SizeMode = PictureBoxSizeMode.StretchImage;
                this.Controls.Add(picture);
            }
        }
        public void ImageEvent(object sender, EventArgs e)
        {
            MessageBox.Show("lol");
        }
    }
}
